// Include Gulp
var gulp = require('gulp');

// Include our plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var dirSync = require( 'gulp-directory-sync');
var exec = require('child_process').exec;

/**********************/
/*  SASS compilation  */
/**********************/

gulp.task('sass', function() {
    return gulp.src('dev/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist'));
});


/**************************/
/*  Image directory sync  */
/**************************/

gulp.task( 'sync', function() {
    return gulp.src( '' )
        .pipe(dirSync( 'dev/img', 'dist', { printSummary: true, ignore: ['inversionz-webfont.eot',
                                                                         'inversionz-webfont.svg',
                                                                         'inversionz-webfont.ttf',
                                                                         'inversionz-webfont.woff',
                                                                         'inversionz-webfont.woff2',
                                                                         'mock.json',
                                                                         'newsreal.min.css',
                                                                         'newsreal.min.js',
                                                                         'static.webm',
                                                                         'inconsolata-regular-webfont.eot',
                                                                         'inconsolata-regular-webfont.svg',
                                                                         'inconsolata-regular-webfont.ttf',
                                                                         'inconsolata-regular-webfont.woff',
                                                                         'inconsolata-regular-webfont.woff2'
                                                                         ] }) );
});


/***********************/
/*  Minify Javascript  */
/***********************/

gulp.task('minifyJS', ['browserify'], function(){
    return gulp.src('dist/newsreal.min.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/'));
});


/****************/
/*  Browserify  */
/****************/

gulp.task('browserify', function(cb){
    return browserify('dev/js/newsreal.js').bundle()
        .pipe(source('newsreal.min.js'))
        .pipe(gulp.dest('dist/'));
    cb(err);
});


/*****************/
/*  Watch tasks  */
/*****************/

// Watch Files For Changes
gulp.task('watch', function(){
    gulp.watch('*/**/*.scss', ['sass']);
    gulp.watch('dev/img/*', ['sync']);
    gulp.watch('dev/js/**/*.js', ['browserify','minifyJS']);
});

/******************/
/*  Package sync  */
/******************/

gulp.task('packageSync', function(){
    gulp.src(['dist/*'])
        .pipe(gulp.dest('../newsreal-drive/public/'));
})

/****************************/
/*  Sync with google drive  */
/****************************/

gulp.task('grive', function(cb){
    exec('cd ../newsreal-drive && grive', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
})

/***************************/
/*  Package sync and send  */
/***************************/

gulp.task('package', ['packageSync', 'grive']);

/******************/
/*  Default task  */
/******************/

gulp.task('default', ['sass', 'sync', 'browserify', 'minifyJS', 'watch']);