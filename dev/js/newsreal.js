// Author: Sean Idzenga
// Version: 1.0
// Date: Oct 19, 2015
window.$ = require('jquery');
require('./debounce.js');
require('./getPosts.js');
require('./navigationMenu.js');
require('./posts.js');
require('./header.js');
require('./contactUs.js');
require('./episodes.js');
require('./randomWallpaper.js');

// for live-mode the url can be relative, but private blogs have feeds disabled
var feedResource = 'https://newsreal2014.blogspot.com/feeds/posts/default?alt=json&max-results=9999';
var pageResource = 'https://newsreal2014.blogspot.com/feeds/pages/default?alt=json&max-results=9999';
// var feedResource = 'https://aa36716ef7408eff16706661bc5db200c32d1705.googledrive.com/host/0B2jaROMkLN_QMDVjYmV0WXN4Vjg/mock.json';
// var pageResource = 'https://aa36716ef7408eff16706661bc5db200c32d1705.googledrive.com/host/0B2jaROMkLN_QMDVjYmV0WXN4Vjg/mock2.json';

/* Pre-load steps */

wallpaper.randomize();

if(postBuilder._isHome()){
    
    $('.main').addClass('home');

} else if (postBuilder._isPostDetail()){

    $('.main').addClass('post-detail');
    addStick();

} else if (postBuilder._isPage()){
    
    $('.main').addClass('page');
    addStick();

}

/* post-load steps */
document.addEventListener("DOMContentLoaded", function(){

    // primer the charger
    navigationMenu._responsiveHide();
    // throw the fucker
    window.addEventListener('resize', navigationMenu._responsiveHide);

    $('.collapseButton').click(function(){
        $('nav').toggleClass('hide');
    });

    if(postBuilder._isHome()){

        document.addEventListener('scroll', sticky);

    	loadJSON(feedResource, 
        	function(data){

        		postBuilder.build(data.feed.entry);
        		window.postFeed = data.feed;

        	}, 
            function(xhr){

            	console.log(xhr); 
                window.postFeed = {fail:true};
            }
        );

    } else if (postBuilder._isPostDetail()) {

        loadJSON(feedResource,
            function(data){

                postBuilder.buildDetail(data.feed.entry);
                window.postFeed = data.feed;

            },
            function(xhr){

                console.log(xhr);
                window.postFeed = {fail:true};
            }
        );

    } else if (postBuilder._isPage()) {

        loadJSON(pageResource,
            function(data){

                postBuilder.buildPage(data.feed.entry);
                window.pageFeed = data.feed;
                navigationMenu._setActive();

                if (episodeList._isEpisodes()){

                    loadJSON(feedResource,
                        function(data){

                            episodeList.episodeContainer = document.getElementById('episodes');
                            episodeList.build(data.feed.entry);
                            window.postFeed = data.feed;
                        },
                        function(xhr){
                            console.log(xhr);
                            window.postFeed = {fail:true};
                        }
                    );
                }
            },
            function(xhr){

                console.log(xhr);
                window.pageFeed = {fail:true};
            }
        );
    }
});