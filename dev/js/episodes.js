window.episodeList = {

	episodeContainer: {},

	entries: {},

	years: [],

	build: function(feed){

		for (var i = 0; i < feed.length; i++){

			if(this._isCategory('newsreal',feed[i].category)){
				
				if( typeof this.entries[this._getYear(feed[i])] === 'undefined' ){
					this.entries[this._getYear(feed[i])] = [];
					this.years.push(this._getYear(feed[i]));
				}
				this.entries[this._getYear(feed[i])].push(this._makeEntry(feed[i]));		
			}
		}

		for (var j = 0; j < this.years.length; j++){

			var thisYear = this.years[j];

			var div = document.createElement('div');
			div.id = thisYear;
			div.className = 'year ';

			var h3 = document.createElement('h3');
			h3.innerHTML = thisYear;

			div.appendChild(h3);

			for (var k = 0; k < this.entries[thisYear].length; k++){
				div.appendChild(this.entries[thisYear][k]);
			}

			this.episodeContainer.appendChild(div);

		}


	},

	_makeEntry: function(entry){


		var anchor = document.createElement('a');
		anchor.href = this._getEnclosureLink(entry.link);
		anchor.id = this._getEntryId(entry.id.$t);
		anchor.innerHTML = 'NewsReal - ' + entry.title.$t + ' - ' + this._formatDate(entry.published.$t);
		// this.episodeContainer.appendChild(anchor);
		return anchor;

	},

	_getEnclosureLink: function(links){

		var rtrnString = '';
		
		for (var i = 0; i < links.length; i++){
			if(links[i].rel === 'enclosure'){
				rtrnString = links[i].href;
				break;
			}
		}

		return rtrnString;
	},

	_formatDate : function(inDate){

		var pubDate = new Date(inDate);
		var monthVal = pubDate.getMonth() + 1;
		var month = monthVal.toString();
		var date = pubDate.getDate().toString();

		if (month.length < 2){
			month = '0' + month;
		}

		if (date.length < 2){
			date = '0' + date;
		}

		return pubDate.getFullYear() + '-' + month + '-' + date;

	},

	_isCategory : function(searchTerm, categories){

		var rtrnBool;

		if (typeof categories !== "undefined"){
			
			for (k = 0; k < categories.length; k++){
				if (categories[k].term === searchTerm){
					rtrnBool = true;
					break;
				}
			}
		} else {
			rtrnBool = false;
		}

		return rtrnBool;
	},

	_isEpisodes : function(){
		return (window.location.pathname).match(/\/p\/episodes.html/i) !== null;
	},

	_getEntryId : function(inString){

		var rtrnString = inString.match(/(?:post-)(\d+)/i);
		return rtrnString[1];
	},

	_getYear : function(entry){

		var year = new Date(entry.published.$t);
		return year.getFullYear();
	}

}